package timrensen.nl.qrcode;

import org.opencv.core.Point;

public class Marker {
    Point p0, p1, p2, p3;

    public Marker(Point p0, Point p1, Point p2, Point p3) {
        this.p0 = p0;
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
    }

    public Point[] getPoints() {
        return new Point[] { p0, p1, p2, p3 };
    }

    public Point getPoint(int id) {
        return getPoints()[id];
    }
}
