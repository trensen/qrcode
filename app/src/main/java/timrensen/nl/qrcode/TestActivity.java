package timrensen.nl.qrcode;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.Arrays;

import timrensen.nl.decoder.Decoder;

public class TestActivity extends ActionBarActivity implements CameraBridgeViewBase.CvCameraViewListener2 {

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch(status) {
                case LoaderCallbackInterface.SUCCESS: {
                    mOpenCvCameraView.enableView();
                    break;
                }
                default: {
                    super.onManagerConnected(status);
                }
            }
        }
    };
    private JavaCameraView mOpenCvCameraView;

    private Mat marker;
    private int markerSize = 14;

    private int qrCodeDetected = 0;

    private Size blurSize = new Size(3, 3);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        mOpenCvCameraView = (JavaCameraView) findViewById(R.id.TestActivityCameraView);
        mOpenCvCameraView.setMaxFrameSize(640, 480);
        //mOpenCvCameraView.setMaxFrameSize(1280, 720);

        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_test, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_9, this, mLoaderCallback);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mOpenCvCameraView != null) {
            mOpenCvCameraView.disableView();
        }
    }

    @Override
    public void onCameraViewStarted(int width, int height) {}

    @Override
    public void onCameraViewStopped() {}

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        Mat mat = inputFrame.gray().clone();
        Mat rgb = inputFrame.rgba().clone();

        mat = Otsu.threshold(mat);
        Mat thresh = mat.clone();

        ArrayList<MatOfPoint> contours = new ArrayList<>();
        Imgproc.findContours(mat, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

        if(contours.size() > 0) {
            ArrayList<MatOfPoint2f> apprPolygons = createPolygonApproximations(contours);
            ArrayList<Marker> markers = selectMarkers(thresh, apprPolygons);

            if(markers.size() == 3) {
                Marker[] dMarkers = distinguishMarkers(markers);

                if (dMarkers.length == 3) {

                    dMarkers = rearrangeMarkerPoints(dMarkers);
                    dMarkers = stabilizeMarkerPoints(dMarkers);

                    Point top = selectCenterPoint(dMarkers[0]);
                    Point right = selectCenterPoint(dMarkers[1]);
                    Point bottom = selectCenterPoint(dMarkers[2]);

                    Core.circle(rgb, top, 6, new Scalar(255, 0, 0, 255), -1);
                    Core.circle(rgb, right, 6, new Scalar(0, 255, 0, 255), -1);
                    Core.circle(rgb, bottom, 6, new Scalar(0, 0, 255, 255), -1);

                    Point[] outside = getOutsidePoints(dMarkers);

                    for (Point p : outside) {
                        Core.circle(rgb, p, 6, new Scalar(255, 168, 0, 255), -1);
                    }

                    if(outside.length == 4) {

                        MatOfPoint2f src = new MatOfPoint2f(outside[0], outside[1], outside[3], outside[2]);
                        MatOfPoint2f dst = new MatOfPoint2f(new Point(0, 0), new Point(210, 0), new Point(210, 210), new Point(0, 210));

                        Mat transform = Imgproc.getPerspectiveTransform(src, dst);

                        Mat unWarpedMarker = new Mat(210, 210, thresh.type());
                        Imgproc.warpPerspective(thresh, unWarpedMarker, transform, new Size(210, 210));

                        Imgproc.resize(unWarpedMarker, unWarpedMarker, new Size(21, 21));
                        Otsu.threshold(unWarpedMarker);

                        if(qrCodeDetected < 30) {
                            qrCodeDetected++;
                        }

                        if(qrCodeDetected == 30) {
                            qrCodeDetected = 0;

                            final int[][] qrCode = convertToIntArray(unWarpedMarker);

                            this.runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast.makeText(TestActivity.this, Decoder.decode(qrCode), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                }
            }
        }

        return rgb;
    }

    public ArrayList<MatOfPoint2f> createPolygonApproximations(ArrayList<MatOfPoint> contours) {
        ArrayList<MatOfPoint2f> result = new ArrayList<>();

        for (MatOfPoint contour : contours) {
            MatOfPoint2f approx = new MatOfPoint2f();
            Imgproc.approxPolyDP(new MatOfPoint2f(contour.toArray()), approx, 3, true);
            result.add(approx);
        }

        return result;
    }


    public ArrayList<Marker> selectMarkers(Mat thresholdImage, ArrayList<MatOfPoint2f> candidates) {
        // Only squares
        for (int i = candidates.size()-1; i >= 0; i--) {
            if(candidates.get(i).size().height != 4) {
                candidates.remove(i);
            }
        }

        // Only squares with two children
        int[] scores = new int[candidates.size()];
        for(int i = candidates.size()-1; i > 0; i--) {
            for(int j = i-1; j >= 0; j--) {
                if(allPointsInPolygon(candidates.get(i).toArray(), candidates.get(j).toArray())) {
                    scores[i]++;
                }

                if(allPointsInPolygon(candidates.get(j).toArray(), candidates.get(i).toArray())) {
                    scores[j]++;
                }
            }

            if(scores[i] != 2)
                candidates.remove(i);

            if(i == 1 && scores[0] != 2)
                candidates.remove(0);
        }

        for (int i = candidates.size()-1; i >= 0; i--) {
            Point[] points = candidates.get(i).toArray();

            MatOfPoint2f src = new MatOfPoint2f(points[0], points[1], points[2], points[3]);
            MatOfPoint2f dst = new MatOfPoint2f(new Point(0, markerSize*10), new Point(0, 0), new Point(markerSize*10, 0), new Point(markerSize*10, markerSize*10));

            Mat transform = Imgproc.getPerspectiveTransform(src, dst);

            Mat unWarpedMarker = new Mat(markerSize*10, markerSize*10, thresholdImage.type());
            Imgproc.warpPerspective(thresholdImage, unWarpedMarker, transform, new Size(markerSize*10, markerSize*10));

            Imgproc.resize(unWarpedMarker, unWarpedMarker, new Size(markerSize, markerSize));
            Imgproc.threshold(unWarpedMarker, unWarpedMarker, 0, 255, Imgproc.THRESH_OTSU);

            Mat subtraction = new Mat();
            Core.subtract(unWarpedMarker, templateFinderPattern(), subtraction);

            if(Core.countNonZero(subtraction) > 0.15*markerSize*markerSize)
                candidates.remove(i);
        }

        ArrayList<Marker> result = new ArrayList<>();
        for(MatOfPoint2f markerPoints : candidates) {
            Point[] points = markerPoints.toArray();
            result.add(new Marker(points[0], points[1], points[2], points[3]));
        }

        return result;
    }


    private Marker[] distinguishMarkers(ArrayList<Marker> markers) {
        ArrayList<Point> centerPoints = selectCenterPoints(markers);
        ArrayList<Marker> result = new ArrayList<>();

        Line l1 = new Line(centerPoints.get(0), centerPoints.get(1));
        Line l2 = new Line(centerPoints.get(1), centerPoints.get(2));
        Line l3 = new Line(centerPoints.get(2), centerPoints.get(0));

        double range1 = Pythagoras.getDistance(centerPoints.get(0), centerPoints.get(1)) / 20;
        double range2 = Pythagoras.getDistance(centerPoints.get(1), centerPoints.get(2)) / 20;
        double range3 = Pythagoras.getDistance(centerPoints.get(2), centerPoints.get(0)) / 20;

        for(Marker marker : markers) {
            int matchingPoints = 0;
            for(Point p : marker.getPoints()) {
                if(l1.pointOnLine(p, range1) || l2.pointOnLine(p, range2) || l3.pointOnLine(p, range3))
                    matchingPoints++;
            }

            if(matchingPoints == 0) {
                result.add(marker);
            }
        }

        if(!result.isEmpty()) {

            Marker top = result.get(0);

            ArrayList<Marker> bottomAndRight = new ArrayList<>(markers);
            bottomAndRight.remove(top);

            if (bottomAndRight.size() == 2) {
                centerPoints = selectCenterPoints(bottomAndRight);
                Line line = new Line(centerPoints.get(0), centerPoints.get(1));

                double dist = line.distanceToPoint(selectCenterPoint(top), false);

                boolean posDist = dist > 0;

                Marker right = bottomAndRight.get(0);
                Marker bottom = bottomAndRight.get(1);

                if (!posDist) {
                    right = bottomAndRight.get(1);
                    bottom = bottomAndRight.get(0);
                }

                result.add(right);
                result.add(bottom);
            }
        }

        return result.toArray(new Marker[result.size()]);
    }



    public Marker[] rearrangeMarkerPoints(Marker[] markers) {
        ArrayList<Point> centers = selectCenterPoints(new ArrayList<>(Arrays.asList(markers)));
        Line diagonal = new Line(centers.get(1), centers.get(2));
        double range = Pythagoras.getDistance(centers.get(1), centers.get(2)) / 20;

        // All points of top-right marker
        Point[] points = new Point[4];
        double maxDistance = Double.NEGATIVE_INFINITY;
        for(Point p : markers[1].getPoints()) {
            if (diagonal.pointOnLine(p, range)) {
                double dist = Pythagoras.getDistance(p, centers.get(2));

                if(maxDistance == Double.NEGATIVE_INFINITY) {
                    points[1] = p;
                    maxDistance = dist;
                } else if(maxDistance < dist) {
                    points[3] = points[1];
                    points[1] = p;
                } else {
                    points[3] = p;
                }
            } else {
                int signDiagonal = (int) Math.signum(diagonal.distanceToPoint(p, false));
                int signTopLeft  = (int) Math.signum(diagonal.distanceToPoint(centers.get(0), false));

                if(signDiagonal == signTopLeft) {
                    points[0] = p;
                } else {
                    points[2] = p;
                }
            }
        }
        markers[1] = new Marker(points[0], points[1], points[2], points[3]);

        // All points of bottom-left marker
        points = new Point[4];
        maxDistance = Double.NEGATIVE_INFINITY;
        for(Point p : markers[2].getPoints()) {
            if (diagonal.pointOnLine(p, range)) {
                double dist = Pythagoras.getDistance(p, centers.get(1));

                if(maxDistance == Double.NEGATIVE_INFINITY) {
                    points[3] = p;
                    maxDistance = dist;
                } else if(maxDistance < dist) {
                    points[1] = points[3];
                    points[3] = p;
                } else {
                    points[1] = p;
                }
            } else {
                int signDiagonal = (int) Math.signum(diagonal.distanceToPoint(p, false));
                int signTopLeft  = (int) Math.signum(diagonal.distanceToPoint(centers.get(0), false));

                if(signDiagonal == signTopLeft) {
                    points[0] = p;
                } else {
                    points[2] = p;
                }
            }
        }
        markers[2] = new Marker(points[0], points[1], points[2], points[3]);


        // Top-left marker
        points = new Point[4];
        int[] scores = new int[4];
        for(int i = 0; i < 3; i++) {
            for(int j = i+1; j < 4; j++) {
                Line l = new Line(markers[0].getPoint(i), markers[0].getPoint(j));
                Point intersection = l.intersectionWith(diagonal);
                double distI = Pythagoras.getDistance(markers[0].getPoint(i), intersection);
                double distJ = Pythagoras.getDistance(markers[0].getPoint(j), intersection);

                if(distI < distJ)
                    scores[i]++;
                else
                    scores[j]++;
            }
        }

        for(int i = 0; i < 4; i++) {
            switch(scores[i]) {
                case 0: points[0] = markers[0].getPoint(i); break;
                case 1: points[1] = markers[0].getPoint(i); break;
                case 2: points[3] = markers[0].getPoint(i); break;
                case 3: points[2] = markers[0].getPoint(i); break;
            }
        }


        Line otherDiagonal = new Line(points[0], points[2]);
        int signTopRight  = (int) Math.signum(otherDiagonal.distanceToPoint(centers.get(1), false));
        int signOtherDiag = (int) Math.signum(otherDiagonal.distanceToPoint(points[3], false));

        if(signTopRight == signOtherDiag) {
            Point temp = points[3];
            points[3] = points[1];
            points[1] = temp;
        }

        markers[0] = new Marker(points[0], points[1], points[2], points[3]);

        return markers;
    }


    private Marker[] stabilizeMarkerPoints(Marker[] dMarkers) {
        Marker topLeft    = dMarkers[0];
        Marker topRight   = dMarkers[1];
        Marker bottomLeft = dMarkers[2];

        Line hor1 = new Line(new Point[] {  topLeft.getPoint(0),    topLeft.getPoint(1),
                                            topRight.getPoint(0),   topRight.getPoint(1)});

        Line hor2 = new Line(new Point[] {  topLeft.getPoint(3),    topLeft.getPoint(2),
                                            topRight.getPoint(3),   topRight.getPoint(2)});

        Line ver1 = new Line(new Point[] {  topLeft.getPoint(0),    topLeft.getPoint(3),
                                            bottomLeft.getPoint(0), bottomLeft.getPoint(3)});

        Line ver2 = new Line(new Point[] {  topLeft.getPoint(1),    topLeft.getPoint(2),
                                            bottomLeft.getPoint(1), bottomLeft.getPoint(2)});

        Line diag = new Line(new Point[] {  bottomLeft.getPoint(3), bottomLeft.getPoint(1),
                                            topRight.getPoint(3),   topRight.getPoint(1)});

        // Top-Left
        Point tl0 = hor1.intersectionWith(ver1);
        Point tl1 = hor1.intersectionWith(ver2);
        Point tl2 = hor2.intersectionWith(ver2);
        Point tl3 = hor2.intersectionWith(ver1);

        // Top-Right
        Point tr0 = dMarkers[1].getPoint(0);
        Point tr1 = hor1.intersectionWith(diag);
        Point tr2 = dMarkers[1].getPoint(2);
        Point tr3 = hor2.intersectionWith(diag);

        // Bottom-Left
        Point bl0 = dMarkers[2].getPoint(0);
        Point bl1 = ver2.intersectionWith(diag);
        Point bl2 = dMarkers[2].getPoint(2);
        Point bl3 = ver1.intersectionWith(diag);

        dMarkers[0] = new Marker(tl0, tl1, tl2, tl3);
        dMarkers[1] = new Marker(tr0, tr1, tr2, tr3);
        dMarkers[2] = new Marker(bl0, bl1, bl2, bl3);

        return dMarkers;
    }


    public Point[] getOutsidePoints(Marker[] markers) {

        Line diagonalTopLeft  = new Line(markers[0].getPoint(0), markers[0].getPoint(2));
        Line verticalRight    = new Line(markers[1].getPoint(1), markers[1].getPoint(2));
        Line horizontalBottom = new Line(markers[2].getPoint(2), markers[2].getPoint(3));

        Point inter1 = diagonalTopLeft.intersectionWith(verticalRight);
        Point inter2 = diagonalTopLeft.intersectionWith(horizontalBottom);

        double avgX = (inter1.x + inter2.x) / 2.0d;
        double avgY = (inter1.y + inter2.y) / 2.0d;

        return new Point[] { markers[0].getPoint(0), markers[1].getPoint(1),
                             markers[2].getPoint(3), new Point(avgX, avgY)};
    }


    private Point selectCenterPoint(Marker marker) {
        Point[] points = marker.getPoints();

        Line l1 = new Line(points[0], points[2]);
        Line l2 = new Line(points[1], points[3]);

        return l1.intersectionWith(l2);
    }


    private ArrayList<Point> selectCenterPoints(ArrayList<Marker> markers) {
        ArrayList<Point> result = new ArrayList<>();

        for (Marker marker : markers) {
           result.add(selectCenterPoint(marker));
        }

        return result;
    }


    public boolean isPointInPolygon(Point point, Point[] polygon) {
        boolean result = false;
        int i, j;

        for (i = 0, j = polygon.length-1; i < polygon.length; j = i++) {
            if ( ((polygon[i].y>point.y) != (polygon[j].y>point.y)) &&
                    (point.x < (polygon[j].x-polygon[i].x) * (point.y-polygon[i].y) / (polygon[j].y-polygon[i].y) + polygon[i].x) )
                result = !result;
        }

        return result;
    }

    private boolean allPointsInPolygon(Point[] parent, Point[] potentialChild) {
        for(Point p : potentialChild) {
            if(!isPointInPolygon(p, parent))
                return false;
        }

        return true;
    }


    private Mat templateFinderPattern() {
        if(marker == null) {
            int seventh = markerSize/7;
            //  20 = seventh
            //  40 = 2*seventh
            // 100 = 5*seventh
            // 120 = 6*seventh

            marker = new Mat(markerSize, markerSize, 0);

            for (int x = seventh; x < 2*seventh; x++) {
                for (int y = seventh; y < 6*seventh; y++) {
                    marker.put(x, y, 255.0);
                }
            }
            for (int x = 5*seventh; x < 6*seventh; x++) {
                for (int y = seventh; y < 6*seventh; y++) {
                    marker.put(x, y, 255.0);
                }
            }
            for (int y = seventh; y < 2*seventh; y++) {
                for (int x = 2*seventh; x < 5*seventh; x++) {
                    marker.put(x, y, 255.0);
                }
            }
            for (int y = 5*seventh; y < 6*seventh; y++) {
                for (int x = 2*seventh; x < 5*seventh; x++) {
                    marker.put(x, y, 255.0);
                }
            }
        }

        return marker;
    }

    public int[][] convertToIntArray(Mat qrCode) {
        int[][] result = new int[21][21];

        Core.divide(255d, qrCode, qrCode);
        for(int row = 0; row < 21; row++) {
            for(int column = 0; column < 21; column++) {
                result[row][column] = (qrCode.get(row, column)[0] == 0) ? 1 : 0;
            }
        }

        return result;
    }
}