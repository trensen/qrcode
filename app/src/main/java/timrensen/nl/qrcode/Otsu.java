package timrensen.nl.qrcode;

import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

public class Otsu {
    private static final MatOfInt histSize = new MatOfInt(256);
    private static final MatOfInt channels = new MatOfInt(0);
    private static final MatOfFloat histRange = new MatOfFloat(0, 256);


    public static Mat threshold(Mat input) {
        Mat hist = new Mat();
        List<Mat> list = new ArrayList<>();
        list.add(input);
        Imgproc.calcHist(list, channels, new Mat(), hist, histSize, histRange, false);

        double totalNumberOfPixels = input.size().area();
        double[] sumNumberOfPixels = new double[256];
        double[] sumGreyValues = new double[256];
        sumNumberOfPixels[0] = hist.get(0,0)[0];
        sumGreyValues[0] = 0;
        for(int i = 1; i < 256; i++) {
            sumNumberOfPixels[i] = sumNumberOfPixels[i-1] + hist.get(i,0)[0];
            sumGreyValues[i] = sumGreyValues[i-1] + i * hist.get(i,0)[0];
        }

        int bestThreshold = -1;
        double maxVariance = 0;

        for(int thresh = 0; thresh < 256; thresh++) {
            double pixelsBackground = sumNumberOfPixels[thresh];
            double pixelsForeground = totalNumberOfPixels - pixelsBackground;

            double weightBackground = pixelsBackground / totalNumberOfPixels;
            double weightForeground = pixelsForeground / totalNumberOfPixels;

            double sumBackground = sumGreyValues[thresh];
            double sumForeground = (thresh > 0) ? sumGreyValues[255] - sumGreyValues[thresh] : sumGreyValues[255];

            double meanBackground = sumBackground / pixelsBackground;
            double meanForeground = sumForeground / pixelsForeground;

            double betweenClassVariance = weightBackground * weightForeground *
                                        Math.pow(meanBackground - meanForeground, 2);

            if(betweenClassVariance > maxVariance) {
                maxVariance = betweenClassVariance;
                bestThreshold = thresh;
            }
        }

        Mat result = new Mat();

        Imgproc.threshold(input, result, bestThreshold, 255, Imgproc.THRESH_BINARY);

        return result;
    }
}
