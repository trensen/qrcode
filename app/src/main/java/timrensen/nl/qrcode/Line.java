package timrensen.nl.qrcode;

import org.opencv.core.Point;

/**
 *   Ax + By + C = 0
 */
public class Line {
    private Point p1, p2;
    private double A, B, C;

    public Line(Point point1, Point point2) {
        this.p1 = point1;
        this.p2 = point2;

        calculateABC();
    }

    /**
     * Creates the best fitting line based on the given points.
     * @param points Array of points.
     */
    public Line(Point[] points) {
        this(points, true);
    }

    private Line(Point[] points, boolean first) {
        if(points == null || points.length <= 1)
            throw new IllegalArgumentException("Array should contain at least two points.");

        double mX = 0, mY = 0, mXX = 0, mXY = 0;
        for(Point p : points) {
            mX  += p.x;
            mY  += p.y;
            mXX += p.x * p.x;
            mXY += p.x * p.y;
        }

        mX  /= (double) points.length;
        mY  /= (double) points.length;
        mXX /= (double) points.length;
        mXY /= (double) points.length;

        this.p1 = new Point(mX, mY);
        this.p2 = new Point(mXX / mX, mXY / mX);

        calculateABC();

        if(first && Math.abs(getM()) > 1) {
            Line perpendicular = new Line(flipPoints(points), false);
            if(perpendicular.isHorizontal()) {
                this.A = 1;
                this.B = 0;
                this.C = perpendicular.getC();
                return;
            }

            Point xAxis = perpendicular.intersectionWith(new Line(new Point(0,0), new Point(1,0)));
            Point yAxis = perpendicular.intersectionWith(new Line(new Point(0,0), new Point(0,1)));

            Line result = new Line(flipPoint(xAxis), flipPoint(yAxis));

            this.A = result.getA();
            this.B = result.getB();
            this.C = result.getC();
        }
    }

    private void calculateABC() {
        // Vertical line
        if(p1.x == p2.x) {
            A = 1;
            B = 0;
            C = -p1.x;
            return;
        }

        // Determine y = mx + b = (n/d)x + b
        // Numerator
        double n = (p2.y - p1.y);

        // Denominator
        double d = (p2.x - p1.x);

        double m = n / d;
        double b = p1.y - m * p1.x;

        // Rewrite y = (n/d)x + b  to  -nx + dy = db
        A = -n;
        B =  d;
        C = -d*b;
    }


    /**
     * http://jccc-mpg.wikidot.com/intersections-and-collision-detection
     */
    public Point intersectionWith(Line line) /*throws Exception*/ {
        // this: Ax + By + C = 0
        // line: Dx + Ey + F = 0
        if(this.getB()*line.getA() - this.getA()*line.getB() == 0)
            throw new IllegalArgumentException("Lines are parallel!");

        // x = -(BF − CE) / (BD − AE)
        double xi = -(this.getB()*line.getC() - this.getC()*line.getB()) /
                     (this.getB()*line.getA() - this.getA()*line.getB());

        // y = -(CD - AF) / (BD - AE)
        double yi = -(this.getC()*line.getA() - this.getA()*line.getC()) /
                     (this.getB()*line.getA() - this.getA()*line.getB());

        return new Point(xi, yi);
    }


    public boolean pointOnLine(Point point, double epsilon) {
        return this.distanceToPoint(point) < epsilon;
    }


    /**
     * Combination of:
     * - http://www.intmath.com/plane-analytic-geometry/perpendicular-distance-point-line.php
     * - http://dsynflo.blogspot.nl/2014/10/opencv-qr-code-detection-and-extraction.html
     */
    public double distanceToPoint(Point point, boolean... absolute) {
        boolean abs = true;
        if(absolute.length == 1)
            abs = absolute[0];

        double AxByC = this.getA() * point.x + this.getB() * point.y + this.getC();
        if(abs)
            AxByC = Math.abs(AxByC);

        return AxByC / Math.sqrt(this.getA() * this.getA() + this.getB() * this.getB());
    }

    public double getA() {
        return A;
    }

    public double getB() {
        return B;
    }

    public double getC() {
        return C;
    }

    public double getM() {
        if(isVertical())
            return Double.POSITIVE_INFINITY;

        return -getA()/getB();
    }

    private boolean isVertical() {
        return getA() == 1 && getB() == 0;
    }

    private boolean isHorizontal() {
        return getA() == 0 && getB() == 1;
    }


    private Point flipPoint(Point point) {
        double d1 = point.y;
        double d2 = point.x;
        return new Point(d1, d2);
    }

    private Point[] flipPoints(Point[] points) {
        Point[] result = new Point[points.length];

        for(int i = 0; i < points.length; i++) {
            result[i] = flipPoint(points[i]);
        }

        return result;
    }
}
