package timrensen.nl.decoder;

public class Decoder {
    public static final String FORMAT_NOT_SUPPORTED   = "Format currently not supported.";
    public static final String ENCODING_NOT_SUPPORTED = "Encoding currently not supported.";

    public static String decode(int[][] qrCode) {
        String formatInfo = "" +
                qrCode[8][0] + qrCode[8][1] + qrCode[8][2] + qrCode[8][3] + qrCode[8][4] +
                qrCode[8][5] + qrCode[8][7] + qrCode[8][8] + qrCode[7][8] + qrCode[5][8] +
                qrCode[4][8] + qrCode[3][8] + qrCode[2][8] + qrCode[1][8] + qrCode[0][8];

        String[][] supportedFormats = new String[4][8];

        // Level L
        supportedFormats[0] = new String[] {"111011111000100", "111001011110011",
                                            "111110110101010", "111100010011101",
                                            "110011000101111", "110001100011000",
                                            "110110001000001", "110100101110110"};
        // Level M
        supportedFormats[1] = new String[] {"101010000010010", "101000100100101",
                                            "101111001111100", "101101101001011",
                                            "100010111111001", "100000011001110",
                                            "100111110010111", "100101010100000"};

        // Level Q
        supportedFormats[2] = new String[] {"011010101011111", "011000001101000",
                                            "011111100110001", "011101000000110",
                                            "010010010110100", "010000110000011",
                                            "010111011011010", "010101111101101"};

        // Level H
        supportedFormats[3] = new String[] {"001011010001001", "001001110111110",
                                            "001110011100111", "001100111010000",
                                            "000011101100010", "000001001010101",
                                            "000110100001100", "000100000111011"};

        int mask = -1;
        for(String[] format : supportedFormats) {
            for (int i = 0; i < format.length; i++) {
                if (format[i].equals(formatInfo)) {
                    mask = i;
                    break;
                }
            }
        }

        if(mask == -1) {
            return FORMAT_NOT_SUPPORTED;
        }

        maskFindingPatterns(qrCode);
        maskTimings(qrCode);
        unmask(qrCode, mask);

        String data = extractData(qrCode);
        return decode(data);
    }

    private static void maskFindingPatterns(int[][] qrCode) {
        int height = qrCode.length;
        int width  = qrCode[0].length;

        for(int row = 0; row < 9; row++) {
            // Top Left
            for(int column = 0; column < 9; column++) {
                qrCode[row][column] = 2;
            }

            // Top Right
            for(int column = width-8; column < width; column++) {
                qrCode[row][column] = 2;
            }
        }

        // Bottom Left
        for(int row = height-8; row < height; row++) {
            for(int column = 0; column < 9; column++) {
                qrCode[row][column] = 2;
            }
        }
    }

    private static void maskTimings(int[][] qrCode) {
        // Horizontal Timing
        for(int column = 0; column < qrCode[0].length; column++) {
            qrCode[6][column] = 2;
        }

        // Vertical Timing
        for(int row = 0; row < qrCode.length; row++) {
            qrCode[row][6] = 2;
        }
    }


    private static void unmask(int[][] qrCode, int mask) {
        for(int row = 0; row < qrCode.length; row++) {
            for(int column = 0; column < qrCode[0].length; column++) {
                boolean switchBit = false;
                switch(mask) {
                    case 0: switchBit = (row + column) % 2 == 0;                                    break;
                    case 1: switchBit = (row) % 2 == 0;                                             break;
                    case 2: switchBit = (column) % 3 == 0;                                          break;
                    case 3: switchBit = (row + column) % 3 == 0;                                    break;
                    case 4: switchBit = (Math.floor(row / 2) + Math.floor(column / 3)) % 2 == 0;    break;
                    case 5: switchBit = ((row * column) % 2) + ((row * column) % 3) == 0;           break;
                    case 6: switchBit = ( ((row * column) % 2) + ((row * column) % 3) ) % 2 == 0;   break;
                    case 7: switchBit = ( ((row + column) % 2) + ((row * column) % 3) ) % 2 == 0;   break;
                }

                if(qrCode[row][column] != 2 && switchBit) {
                    qrCode[row][column] = 1 - qrCode[row][column];
                }
            }
        }
    }


    private static String extractData(int[][] qrCode) {
        String result = "";

        int curX = 20, curY = 20;
        int dy = -1;
        while(curX >= 0 && curY >= 0
                && 0 <= curY && curY <= 20) {

            result += qrCode[curY][curX];
            result += qrCode[curY][--curX];

            curY += dy;
            curX++;

            switch(curY) {
                case -1:
                    curY++;
                    curX -= 2;
                    dy = -dy;
                    break;
                case 21:
                    curY--;
                    curX -= 2;
                    dy = -dy;
                    break;
            }

            // Skip vertical timing
            if(curX == 6)
                curX--;
        }

        // Remove data from Finding Patterns and Timings
        result = result.replace("2", "");

        return result;
    }


    private static String decode(String data) {
        String encodingMode = data.substring(0, 4);

        if(!"0100".equals(encodingMode)) {
            return ENCODING_NOT_SUPPORTED;
        }

        int length = Integer.parseInt(data.substring(4, 12), 2);

        String result = "";
        int beginIndex, byteValue;
        for(int character = 0; character < length; character++) {
            beginIndex = 12 + 8*character;
            if(beginIndex < data.length() && beginIndex + 8 < data.length()) {
                byteValue = Integer.parseInt(data.substring(beginIndex, beginIndex + 8), 2);
                result += (char) byteValue;
            }
        }

        return result;
    }
}
